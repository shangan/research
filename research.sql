/*
Navicat MySQL Data Transfer

Source Server         : hlj
Source Server Version : 50556
Source Host           : localhost:3306
Source Database       : research

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2019-08-20 16:20:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `answer`
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer` (
  `queid` varchar(20) NOT NULL,
  `anid` varchar(20) NOT NULL,
  `ans` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES ('1', 'A', '1');
INSERT INTO `answer` VALUES ('1', 'B', '2');
INSERT INTO `answer` VALUES ('1', 'C', '12');
INSERT INTO `answer` VALUES ('1', 'D', '22');
INSERT INTO `answer` VALUES ('2', 'A', '11');
INSERT INTO `answer` VALUES ('2', 'B', '14');
INSERT INTO `answer` VALUES ('2', 'C', '16');
INSERT INTO `answer` VALUES ('2', 'D', '15');
INSERT INTO `answer` VALUES ('3', '001', '11');
INSERT INTO `answer` VALUES ('3', '002', '12');
INSERT INTO `answer` VALUES ('3', '', '');
INSERT INTO `answer` VALUES ('3', '', '');
INSERT INTO `answer` VALUES ('4', 'A', '12');
INSERT INTO `answer` VALUES ('4', 'B', '13');
INSERT INTO `answer` VALUES ('4', 'C', '14');
INSERT INTO `answer` VALUES ('4', '', '');

-- ----------------------------
-- Table structure for `colu`
-- ----------------------------
DROP TABLE IF EXISTS `colu`;
CREATE TABLE `colu` (
  `cid` varchar(20) NOT NULL,
  `cname` varchar(20) NOT NULL,
  `curl` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of colu
-- ----------------------------
INSERT INTO `colu` VALUES ('1', '权限设置', 'http://localhost:8080/research/QueryMiddle');
INSERT INTO `colu` VALUES ('2', '结果查看', 'http://localhost:8080/research/SelectAnswerServlet');
INSERT INTO `colu` VALUES ('3', '角色管理', 'http://localhost:8080/research/QueryAdmin');
INSERT INTO `colu` VALUES ('4', '用户管理', 'http://localhost:8080/research/QueryUser');
INSERT INTO `colu` VALUES ('5', '题目管理', 'http://localhost:8080/research/QueryQuestion');

-- ----------------------------
-- Table structure for `information`
-- ----------------------------
DROP TABLE IF EXISTS `information`;
CREATE TABLE `information` (
  `id` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `sex` varchar(30) DEFAULT NULL,
  `year` varchar(30) DEFAULT NULL,
  `month` varchar(30) DEFAULT NULL,
  `birthday` varchar(30) DEFAULT NULL,
  `age` int(30) DEFAULT NULL,
  `question` varchar(30) DEFAULT NULL,
  `answer` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of information
-- ----------------------------
INSERT INTO `information` VALUES ('44444444444', 'hhh', '123', '3201005780@qq.com', '男', '1909', '12', '18', '109', '你最喜欢的老师是谁？', 'qq');
INSERT INTO `information` VALUES ('22222222222', '22', '11', '1466926982@qq.com', '男', '1931', '12', '16', '87', '你最喜欢的老师是谁？', 'li');
INSERT INTO `information` VALUES ('66666666666', 'aa', '11', '1466926982@qq.com', '男', '1915', '11', '17', '103', '你最喜欢的老师是谁？', '11');
INSERT INTO `information` VALUES ('18181073474', 'hh', '123456', null, '女', '2008', '12', '15', '11', null, null);

-- ----------------------------
-- Table structure for `manager`
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `mgid` varchar(20) NOT NULL,
  `mgname` varchar(20) NOT NULL,
  `mgpsw` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('18381760064', '猴子', '12322');
INSERT INTO `manager` VALUES ('111', '哈哈', '123');

-- ----------------------------
-- Table structure for `middle`
-- ----------------------------
DROP TABLE IF EXISTS `middle`;
CREATE TABLE `middle` (
  `mgid` varchar(20) NOT NULL,
  `cid` varchar(20) DEFAULT NULL,
  `mcid` varchar(20) NOT NULL,
  PRIMARY KEY (`mcid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of middle
-- ----------------------------
INSERT INTO `middle` VALUES ('111', '1', '1111');
INSERT INTO `middle` VALUES ('111', '2', '1112');
INSERT INTO `middle` VALUES ('111', '3', '1113');
INSERT INTO `middle` VALUES ('111', '4', '1114');
INSERT INTO `middle` VALUES ('111', '5', '1115');
INSERT INTO `middle` VALUES ('1', '2', '12');
INSERT INTO `middle` VALUES ('1', '3', '13');
INSERT INTO `middle` VALUES ('18381760064', '2', '183817600642');
INSERT INTO `middle` VALUES ('18381760064', '4', '183817600644');
INSERT INTO `middle` VALUES ('18381760064', '5', '183817600645');

-- ----------------------------
-- Table structure for `question`
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `queid` varchar(20) NOT NULL,
  `que` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES ('1', '1+1=?');
INSERT INTO `question` VALUES ('2', '4*4=?');
INSERT INTO `question` VALUES ('3', '9*9=？');
INSERT INTO `question` VALUES ('4', '5+9=?');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userid` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `sex` varchar(6) DEFAULT NULL,
  `age` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('18381760064', '侯丽娟', '123', '女', '20');
INSERT INTO `user` VALUES ('123', 'aaaaa', '112222222', '女', '11');
INSERT INTO `user` VALUES ('46566', '我', '111', '女', '12');
INSERT INTO `user` VALUES ('007', 'tom', '123456', 'man', '25');
INSERT INTO `user` VALUES ('101010', 'dd', '1111', '女', '11');
INSERT INTO `user` VALUES ('006', 'mary', '123456', 'woman', '23');
INSERT INTO `user` VALUES ('11111111111', '黄朝阳', '111', '女', '15');
INSERT INTO `user` VALUES ('99999999999', '谢婷', '111111', '女', '12');
INSERT INTO `user` VALUES ('44444444444', 'hhh', '123', '男', '109');
INSERT INTO `user` VALUES ('22222222222', '22', '11', '男', '87');
INSERT INTO `user` VALUES ('66666666666', 'aa', '11', '男', '103');
INSERT INTO `user` VALUES ('18181073474', 'hh', '123456', '女', '11');

-- ----------------------------
-- Table structure for `userque`
-- ----------------------------
DROP TABLE IF EXISTS `userque`;
CREATE TABLE `userque` (
  `queid` varchar(20) NOT NULL,
  `anid` varchar(20) NOT NULL,
  `uid` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userque
-- ----------------------------
INSERT INTO `userque` VALUES ('1', 'A', '006');
INSERT INTO `userque` VALUES ('2', 'B', '006');
INSERT INTO `userque` VALUES ('1', 'A', '007');
INSERT INTO `userque` VALUES ('2', 'C', '007');
INSERT INTO `userque` VALUES ('1', 'B', '13558536637');
INSERT INTO `userque` VALUES ('2', 'C', '13558536637');
INSERT INTO `userque` VALUES ('1', 'A', '11111111111');
INSERT INTO `userque` VALUES ('2', 'A', '11111111111');
INSERT INTO `userque` VALUES ('1', 'A', '99999999999');
INSERT INTO `userque` VALUES ('2', 'A', '99999999999');
INSERT INTO `userque` VALUES ('1', 'C', '22222222222');
INSERT INTO `userque` VALUES ('2', 'B', '22222222222');
INSERT INTO `userque` VALUES ('3', '002', '22222222222');
INSERT INTO `userque` VALUES ('1', 'B', '66666666666');
INSERT INTO `userque` VALUES ('2', 'C', '66666666666');
INSERT INTO `userque` VALUES ('3', '001', '66666666666');
INSERT INTO `userque` VALUES ('1', 'B', '18381760064');
INSERT INTO `userque` VALUES ('2', 'A', '18381760064');
INSERT INTO `userque` VALUES ('3', '001', '18381760064');
