package privilege;

public class LittleNodes {
	private String id;
	private String text;
	private String href;
	public LittleNodes() {
		super();
	}
	public LittleNodes(String id, String text, String href) {
		super();
		this.id = id;
		this.text = text;
		this.href = href;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	
	
}
