package privilege;

import java.util.List;

public class MiddleNodes {
	private String text;
	private List<LittleNodes> items;
	public MiddleNodes() {
		super();
	}
	public MiddleNodes(String text, List<LittleNodes> items) {
		super();
		this.text = text;
		this.items = items;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<LittleNodes> getItems() {
		return items;
	}
	public void setItems(List<LittleNodes> items) {
		this.items = items;
	}
	
	
}
