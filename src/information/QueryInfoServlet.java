package information;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class QueryInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String id=request.getParameter("id");
		InformationJdbc question=new InformationJdbc();
		try{
			List<Information> infoList=question.queryInfoById(id);
			request.setAttribute("infoList",infoList);
			RequestDispatcher dis=request.getRequestDispatcher("Security/index.jsp");
			dis.forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
