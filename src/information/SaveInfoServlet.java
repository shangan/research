package information;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String sex = request.getParameter("sex");
		String a=request.getParameter("age");
		int age=Integer.parseInt(a);
		String year=request.getParameter("year");
		String month=request.getParameter("month");
		String day=request.getParameter("day");
		String question=request.getParameter("question");
		String answer=request.getParameter("answer");
		InformationJdbc q=new InformationJdbc();
		try{
			q.UpdateInfo(id,name,email,year,month,day,sex,age,question,answer);
			RequestDispatcher dis=request.getRequestDispatcher("QueryInfo");
			dis.forward(request, response);
		}catch(Exception e){
			e.printStackTrace()  ;
		} 
	}

}
