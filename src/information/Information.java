package information;

public class Information {
	
	private String id;
	private String name;
	private String password;
	private String email;
	private String sex;
	private String year;
	private String month;
	private String birthday;
	private int age;
	private String question;
	private String answer;
	private String code;
	

	public Information() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Information(String id, String name, String email, String sex, String year, String month, String birthday,
			int age, String question, String answer, String password) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.sex = sex;
		this.year = year;
		this.month = month;
		this.birthday = birthday;
		this.age = age;
		this.question = question;
		this.answer = answer;
		this.password=password;
	}
	public Information(String email) {
		// TODO Auto-generated constructor stub
		super();
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}



	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getSex() {
		return sex;
	}


	public void setSex(String sex) {
		this.sex = sex;
	}


	public String getYear() {
		return year;
	}


	public void setYear(String year) {
		this.year = year;
	}


	public String getMonth() {
		return month;
	}


	public void setMonth(String month) {
		this.month = month;
	}


	public String getBirthday() {
		return birthday;
	}


	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public String getQuestion() {
		return question;
	}


	public void setQuestion(String question) {
		this.question = question;
	}


	public String getAnswer() {
		return answer;
	}


	public void setAnswer(String answer) {
		this.answer = answer;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
