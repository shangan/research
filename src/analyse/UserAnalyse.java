package analyse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import admin.TestPro;
import information.InformationJdbc;
import user.Userjdbc;

public class UserAnalyse {
	
	public static PreparedStatement getCon(String sql) throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/research?characterEncoding=utf-8";
		Connection con = DriverManager.getConnection(url, "root", "123456");
		PreparedStatement pre = con.prepareStatement(sql);
		//con.close();
		return pre;
	}
	
	public String CountMan() throws Exception {
		String sql="select count(*) from user where sex='男'";
		PreparedStatement pre=UserAnalyse.getCon(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countman=res.getString(1);
		return countman;
	}
	public String CountWoman() throws Exception {
		String sql="select count(*) from user where sex='女'";
		PreparedStatement pre=UserAnalyse.getCon(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countman=res.getString(1);
		return countman;
	}
	public String CountTeenager() throws Exception {
		String sql="select count(*) from information where age<18&&age>0";
		PreparedStatement pre=UserAnalyse.getCon(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countteenager=res.getString(1);
		
		return countteenager;
	}
	public String CountAdult() throws Exception {
		String sql="select count(*) from information where age>=18&&age<50";
		PreparedStatement pre=UserAnalyse.getCon(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countadult=res.getString(1);
		 
		return countadult;
	}
	public String CountOld() throws Exception {
		String sql="select count(*) from information where age>=50";
		PreparedStatement pre=UserAnalyse.getCon(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countold=res.getString(1);
		 
		return countold;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
