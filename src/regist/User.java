package regist;

public class User {

	private String userid;
	private String name;
	private String password;
	private String sex;
	private String age;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public User(String userid, String name, String password, String sex, String age) {
		super();
		this.userid = userid;
		this.name = name;
		this.password = password;
		this.sex = sex;
		this.age = age;
	}
	
	
}
