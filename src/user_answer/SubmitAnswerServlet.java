package user_answer;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SubmitAnswerServlet
 */
public class SubmitAnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.setCharacterEncoding("utf-8");// ��ҳ����������ݴ����ʽ
		response.setContentType("text/html;charset=utf-8");// ������ҳ���ı������ʽ
		//String account=request.getParameter("account");
		HttpSession se=request.getSession();
		String account=(String) se.getAttribute("uid");
		try {
			String sql="select * from question ";
			ResultSet rs=DataBaseOperate.selectDB(sql);
			while(rs.next()) {
				String id=rs.getString(1);	
				String option=request.getParameter(id);//���id��rs.getString(1);
				DataBaseOperate.saveAnswer(account, id, option);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect("GetQuesServlet");
	}

}
