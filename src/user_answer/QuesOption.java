package user_answer;

public class QuesOption {
private String quesid;//问题id 意为第几题
private String anid;//选项Id，意为A，B，C，D
private String ans;//选项详情
public QuesOption(String quesid, String anid, String ans) {
	super();
	this.quesid = quesid;
	this.anid = anid;
	this.ans = ans;
}
public QuesOption() {
	super();
	// TODO Auto-generated constructor stub
}
public String getQuesid() {
	return quesid;
}
public void setQuesid(String quesid) {
	this.quesid = quesid;
}
public String getAnid() {
	return anid;
}
public void setAnid(String anid) {
	this.anid = anid;
}
public String getAns() {
	return ans;
}
public void setAns(String ans) {
	this.ans = ans;
}


}
