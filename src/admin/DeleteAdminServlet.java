package admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteAdminServlet
 */
public class DeleteAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("iid");
		Adminjdbc qu=new Adminjdbc();
		try{
			qu.deleteAdmin(id);
			response.sendRedirect("QueryAdmin");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
