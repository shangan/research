package admin;

import java.io.InputStream;
import java.util.Properties;

public class TestPro {
	public static String readProper(String key) throws Exception{
		Properties pro=new Properties();
		Class cls=TestPro.class;
		ClassLoader cl=cls.getClassLoader();
		InputStream is=cl.getResourceAsStream("jdbc.properties");
		pro.load(is);
		String val=pro.getProperty(key);
		return val;
	}
}
