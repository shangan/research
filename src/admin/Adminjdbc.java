package admin;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;

import admin.Manager;

public class Adminjdbc {
	public static ArrayList<Manager> queryAdmin() throws Exception {
		String sql="select * from manager";
		Class.forName(TestPro.readProper("name"));
		String url=TestPro.readProper("url");
		java.sql.Connection con = DriverManager.getConnection(url, TestPro.readProper("username"),TestPro.readProper("psw"));
		java.sql.PreparedStatement pre=con.prepareStatement(sql);
		ResultSet res=pre.executeQuery();
		ArrayList<Manager> list=new ArrayList<Manager>();
		while(res.next()){
			String id=res.getString(1);
			String name=res.getString(2);
			String psw=res.getString(3);
			Manager qu=new Manager(id,name,psw);
			list.add(qu);
		}
		return list;
	}
	
	public void deleteAdmin(String id) throws Exception {
		// TODO Auto-generated method stub
		String sql="delete from manager where mgid=?";
		Class.forName(TestPro.readProper("name"));
		String url=TestPro.readProper("url");
		java.sql.Connection con = DriverManager.getConnection(url, TestPro.readProper("username"), TestPro.readProper("psw"));
		java.sql.PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1,id);
		pre.execute();
		con.close();
	}
	
	
	public ArrayList<Manager> AdminById(String id) throws Exception {
		String sql="select * from manager where mgid=?";
		Class.forName(TestPro.readProper("name"));
		String url=TestPro.readProper("url");
		java.sql.Connection con = DriverManager.getConnection(url, TestPro.readProper("username"), TestPro.readProper("psw"));
		java.sql.PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1, id);
		ResultSet res=pre.executeQuery();
		ArrayList<Manager> list=new ArrayList<Manager>();
		while(res.next()){
			String id1=res.getString(1);
			String name=res.getString(2);
			String psw=res.getString(3);
			Manager qu=new Manager(id1,name,psw);
			list.add(qu);
		}
		con.close();
		return list;
	}
	
	
	public void updateAdmin(String id,String name,String psw) throws Exception{
		String sql="update manager set mgid=?,mgname=?,mgpsw=? where mgid=?";
		Class.forName(TestPro.readProper("name"));
		String url=TestPro.readProper("url");
		java.sql.Connection con = DriverManager.getConnection(url, TestPro.readProper("username"), TestPro.readProper("psw"));
		java.sql.PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1,id);
		pre.setString(2,name);
		pre.setString(3,psw);
		pre.setString(4,id);
		pre.execute();
	}
}
