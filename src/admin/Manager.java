package admin;

public class Manager {
	private String id;
	private String name;
	private String psw;
	
	public Manager() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPsw() {
		return psw;
	}

	public void setPsw(String psw) {
		this.psw = psw;
	}

	public Manager(String id, String name, String psw) {
		super();
		this.id = id;
		this.name = name;
		this.psw = psw;
	}
}
