package control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeletePowerServlet
 */
public class DeletePowerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		String mgid= request.getParameter("mgid");
		String cid= request.getParameter("cid");
		
		MiddleDao md = new MiddleDao();
		String[] cids = cid.split(",");
		//System.out.println(Arrays.toString(cids));
		try {
			for(int i=0;i<cids.length;i++) {
				md.deletePowers(mgid, cids[i]);
			}
		} catch (Exception e) {
			System.out.println("你要解除的权限不存在！！");
		}
		
		request.getRequestDispatcher("QueryMiddle").forward(request, response);
	}
		
		
	}


