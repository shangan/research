package control;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class QueryPowerServlet
 */
public class QueryPowerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mgid = request.getParameter("mgid");
		PowerDao pd = new PowerDao();
		try {
			List<Power> plist= pd.QueryPower(mgid);
			request.setAttribute("plist", plist);
			request.getRequestDispatcher("query.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
