package control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.Manager;




public class QueryMilddleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		Middlejdbc sd=new Middlejdbc();
		try{
			ArrayList<Manager> list = sd.queryMiddle();	
			request.setAttribute("list", list);
			RequestDispatcher dis=request.getRequestDispatcher("Control/index.jsp");
			dis.forward(request,response);
	       }catch(Exception e){
				e.printStackTrace();
			}
		
	}

}
