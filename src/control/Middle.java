package control;

public class Middle {
	String mgid;
	String cid;
	public String getMgid() {
		return mgid;
	}
	public void setMgid(String mgid) {
		this.mgid = mgid;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public Middle(String mgid, String cid) {
		super();
		this.mgid = mgid;
		this.cid = cid;
	}
	
	public Middle(String cid) {
		super();
		this.cid = cid;
	}
	
}
