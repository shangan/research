package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class PowerDao {
	
	//��ѯһ������Ա������Ȩ��
	public List<Power> QueryPower(String mgid) throws Exception {
		
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql = "select * from colu where cid in (select cid from middle where mgid=?)";
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1, mgid);
		ResultSet res = pre.executeQuery();
		List<Power> plist = new ArrayList<Power>();
		while (res.next()) {
			String cid = res.getString(1);
			String cname = res.getString(2);
			String url = res.getString(3);
			Power power = new Power(cid,cname, url);
			plist.add(power);
		}
		con.close();
		return plist;
	}
	
	//��ѯһ������Ա������Ȩ����
	public List<String> QuerypowerNames(String mgid) throws Exception {
		
	//���ݿ�����
	Connection con = DBUtils.getCon();
	
	// ִ��sql���
	String sql = "select * from power where cid in (select cid from middle where mgid=?)";
	PreparedStatement pre = con.prepareStatement(sql);
	pre.setString(1, mgid);
	ResultSet res = pre.executeQuery();
	List<String> nlist = new ArrayList<String>();
	while (res.next()) {
		String cname = res.getString(2);
		nlist.add(cname);
	}
	con.close();
	return nlist;
	}
}
