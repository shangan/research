package control;

import java.util.List;

public class Node {
	private String id;
	private String text;
	private String state;
	private List<Node> children;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	

	public Node(String id, String text, String state, List<Node> children) {
		super();
		this.id = id;
		this.text = text;
		this.state = state;
		this.children = children;
	}

	public Node() {
		super();
	}

}
