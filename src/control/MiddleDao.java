package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class MiddleDao {
	
	
	//���¹���Ա��Ȩ��
	public Middle queryMiddle(String mgid,String cid) throws Exception {
			
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql = "select * from middle where mgid=? and cid=?";
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1, mgid);
		pre.setString(2, cid);
		ResultSet res = pre.executeQuery();
		Middle middle = null;
		while (res.next()) {
			String mmgid = res.getString(1);
			String mcid = res.getString(2);
			middle = new Middle(mmgid, mcid);
		}
		con.close();
		return middle;
	}
	
	//���¹���Ա��Ȩ��
	public void updatePowers(String mgid,String cid) throws Exception {
			
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		MiddleDao md = new MiddleDao();
		Middle middle = md.queryMiddle(mgid, cid);
		//if(middle.getMgid()!=mgid&&middle.getCid()!=cid) {
		// ִ��sql���
		String sql = "insert into middle values(?,?)";
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1, mgid);
		pre.setString(2, cid);
		boolean res = pre.execute();
		//}
		con.close();
	}
	
	
	//ɾ������Ա��Ȩ��
	public void deletePowers(String mgid,String cid) throws Exception {
			
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql = "delete from middle where mcid=?";
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1, mgid+cid);
		boolean res = pre.execute();
		con.close();
	}
}
