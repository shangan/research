package question;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import question.dao.AnswerDao;
import question.dao.QuestionDao;
import question.pojo.Answer;
import question.pojo.Question;

/**
 * Servlet implementation class SelectQuestionServlet
 */
public class SelectQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String queid=request.getParameter("menuname");
		QuestionDao qd=new QuestionDao();
		AnswerDao ad=new AnswerDao();
		try {
			Question question = qd.selectQueById(queid);
			request.setAttribute("q", question);
			List<Answer> alist=ad.selectAnswers(queid);
			request.setAttribute("list", alist);
			request.getRequestDispatcher("Menu/SelectQuestion.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
