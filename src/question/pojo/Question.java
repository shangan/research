package question.pojo;

import java.util.List;

public class Question {
	private String queid;
	private String que;
	private List<Answer> alist;
	public Question() {
		super();
	}
	public Question(String queid, String que, List<Answer> alist) {
		super();
		this.queid = queid;
		this.que = que;
		this.alist = alist;
	}
	public String getQueid() {
		return queid;
	}
	public void setQueid(String queid) {
		this.queid = queid;
	}
	public String getQue() {
		return que;
	}
	public void setQue(String que) {
		this.que = que;
	}
	public List<Answer> getAlist() {
		return alist;
	}
	public void setAlist(List<Answer> alist) {
		this.alist = alist;
	}
}	
