package question.pojo;

public class Answer {
	private String queid;
	private String anid;
	private String ans;
	public Answer() {
		super();
	}
	
	public Answer(String anid, String ans) {
		super();
		this.anid = anid;
		this.ans = ans;
	}

	public Answer(String queid, String anid, String ans) {
		super();
		this.queid = queid;
		this.anid = anid;
		this.ans = ans;
	}
	public String getQueid() {
		return queid;
	}
	public void setQueid(String queid) {
		this.queid = queid;
	}
	public String getAnid() {
		return anid;
	}
	public void setAnid(String anid) {
		this.anid = anid;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	
	
}
