package question;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import question.dao.*;
import question.pojo.*;

public class QueryQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		QuestionDao qd=new QuestionDao();
		try {
			List<Question> qlist = qd.selectAllQues();
			request.setAttribute("list", qlist);
			request.getRequestDispatcher("Menu/index.jsp").forward(request, response);;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
