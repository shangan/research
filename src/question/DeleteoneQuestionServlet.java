package question;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import question.dao.*;
import question.pojo.*;

/**
 * Servlet implementation class DeleteoneQuestionServlet
 */
public class DeleteoneQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String queid = request.getParameter("queid");
		QuestionDao qd = new QuestionDao();
		try {
			Question question = qd.selectQueById(queid);
			request.setAttribute("q", question);
			request.getRequestDispatcher("Menu/deleteque.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
