package question;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import question.dao.QuestionDao;
import question.pojo.Question;

/**
 * Servlet implementation class QueryoneQuestionServlet
 */
public class QueryoneQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String queid = request.getParameter("id");
		QuestionDao qd = new QuestionDao();
		try {
			Question que = qd.selectQueById(queid);
			request.setAttribute("q", que);
			request.getRequestDispatcher("Menu/update.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
