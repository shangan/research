package question;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import question.dao.AnswerDao;
import question.dao.QuestionDao;

/**
 * Servlet implementation class SaveQuestionServlet
 */
public class SaveQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;utf-8");
		
		String queid = request.getParameter("queid");
		String que = request.getParameter("que");
		String anid1=request.getParameter("anid1");
		String ans1=request.getParameter("ans1");
		String anid2=request.getParameter("anid2");
		String ans2=request.getParameter("ans2");
		String anid3=request.getParameter("anid3");
		String ans3=request.getParameter("ans3");
		String anid4=request.getParameter("anid4");
		String ans4=request.getParameter("ans4");
		QuestionDao qd = new QuestionDao();
		AnswerDao ad = new AnswerDao();
		try {
			qd.insertQuestion(queid, que);
			ad.insertAnswer(queid, anid1, ans1);
			ad.insertAnswer(queid, anid2, ans2);
			ad.insertAnswer(queid, anid3, ans3);
			ad.insertAnswer(queid, anid4, ans4);
			response.sendRedirect("QueryQuestion");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
