package question;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import question.dao.AnswerDao;
import question.dao.QuestionDao;
import question.pojo.Question;

/**
 * Servlet implementation class DeleteQuestionServlet
 */
public class DeleteQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		String queid = request.getParameter("id");
		QuestionDao qd = new QuestionDao();
		AnswerDao ad = new AnswerDao();
		try {
			ad.deleteAnswers(queid);
			List<Question> qlist = qd.deleteQueById(queid);
			request.setAttribute("list", qlist);
			request.getRequestDispatcher("Menu/index.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
