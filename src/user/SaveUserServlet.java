package user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import user.Userjdbc;


public class SaveUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String id=request.getParameter("userid");
		String name=request.getParameter("name");
		String psw=request.getParameter("password");
		String sex=request.getParameter("sex");
		String age=request.getParameter("age");
		Userjdbc uj=new Userjdbc();
		try {
			uj.saveUser(id, name,psw, sex, age);
			response.sendRedirect("User/add.jsp");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
