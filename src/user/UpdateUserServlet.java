package user;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import user.Userjdbc;

/**
 * Servlet implementation class UpdateUserServlet
 */
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String id=request.getParameter("userid");
		String name=request.getParameter("name");
		String psw=request.getParameter("password");
		String sex=request.getParameter("sex");
		String age=request.getParameter("age");
		Userjdbc qu=new Userjdbc();
		try{
			qu.updateUser(id,name,psw,sex,age);
			RequestDispatcher dis=request.getRequestDispatcher("QueryUser");
			dis.forward(request,response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
