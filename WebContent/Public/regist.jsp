<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
	<meta charset="UTF-8">
	<title>注册页</title>
	<link rel="stylesheet" type="text/css" href="../Css/registcss.css" />
</head>
<style>
		#tipname{
			padding-left:88px;
		}
		#tippsw{
			padding-left:88px;
		}
		#tiptel{
			padding-left:88px;
		}
		#c1{
			color: #272b37;
			font-size: 20px;
			border: 1px solid #e2e2e2;
			width: 260px;
			height: 40px;
			border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
		}
		
	</style>
<body>
		<header>
			<img src="../img/logo.png" />
			<ul>
				<li><a href="index1.jsp">首页</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">填写问卷</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.jsp">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
		</header>
		<div id="log">
		<form action="http://localhost:8080/research/userRegisterServlet" method="post" id="form">
			<table>
				<tr>
					<td><input id="telnumber" name="userid" type="tel" maxlength="11" placeholder="  请输入手机号码" /></td>
				</tr>
				<tr>
                	<td id="tiptel" style="color:red;"></td>
				</tr>
				<tr>
					<td><input id="username" name="username" type="text" maxlength="10" placeholder="  请输入用户名" onclick="checkTel()"/></td>
				</tr>
				<tr>
                	<td id="tipname" style="color:red;"></td>
				</tr>
				<tr>
					<td><input id="password" name="password" type="password" maxlength="10" placeholder="  请输入密码" onclick="checkName()"/></td>
				</tr>
				<tr>
                	<td id="tippsw" style="color:red;"></td>
				</tr>
			</table>	
				<select id="sex" name="sex" onclick="checkPsw()">
					<option>男</option>
					<option>女</option>
				</select>
				<center id="c1">
						生日:<select id="birth_year" name="year" style="width: 60px;height: 30px;">
								<option ></option>
								<c:forEach var="i" begin="1899" end="2018"> 
									<option><c:out value="${i}"/></option>
								</c:forEach>
							</select>
							<select name="month" style="width: 60px;height: 30px;">
								<option></option>
								<c:forEach var="i" begin="1" end="12"> 
									<option><c:out value="${i}"/></option>
								</c:forEach>
							</select>
							<select name="day"style="width: 60px;height: 30px;">
								<option></option>
								<c:forEach var="i" begin="1" end="31"> 
									<option><c:out value="${i}"/></option>
								</c:forEach>
							</select><br>
				</center>
							<input type="hidden" id="age" value="" name="age">
				<button id="denglu" onclick="check()">注册</button>
			</form>
		</div>
		<footer>
			<p>HXPZLQ版权所有</p>
		</footer>
</body>
<script type="text/javascript">
		function check(){
			var date=new Date();   
			var year=date.getFullYear();
			var brith=document.getElementById("birth_year").value;
			var yearvalue=parseInt(year);
			var birthvalue=parseInt(brith); 
			var age=(yearvalue-birthvalue);
			document.getElementById("age").value=age;
			document.getElementById("year").value=yearvalue;
			var tel = document.getElementById('telnumber').value;
			var username = document.getElementById('username').value;
			var password = document.getElementById('password').value;
			if(username== ''){
		       alert("用户名不能为空！");
		       return;
		   }else if(password== ''){
				alert("密码不能为空！");
				return;
		   }else if(tel.length<11){
				alert("请输入有效的账号！");
				return;
			}else{
			    alert("您的账号安全级别较低，请完善信息！");
				var form=document.getElementById('form');
				form.submit();
				return;
			}
		}
		
	    function checkTel(){
	    	var tel = document.getElementById('telnumber').value;
	    	if(tel == ''){
	    		document.getElementById('tiptel').innerHTML = "账号不能为空";
	    		return;
	    	}else if(tel.length<11){
	    		document.getElementById('tiptel').innerHTML = "请输入有效的账号！";
	    		return;
	    	}else{
	    		document.getElementById('tiptel').innerHTML = "";
	    		return;
	    	}
	    }
	    function checkName(){
	    	var username = document.getElementById('username').value;
	    	if(username == ''){
	    		document.getElementById('tipname').innerHTML = "用户名不能为空";
	    		return;
	    	}else{
	    		document.getElementById('tipname').innerHTML = "";
	    		return;
	    	}
	    }
	    function checkPsw(){
	    	var password = document.getElementById('password').value;
	    	if(password == ''){
	           document.getElementById('tippsw').innerHTML = "密码不能为空";
	           return;
	       }else{
	   		document.getElementById('tippsw').innerHTML = "";
			return;
		}
	    }
	    
</script>
</html>
