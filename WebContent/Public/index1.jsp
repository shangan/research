<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>主页</title>
		<script src="../Js/jquery-3.2.1.js"></script>
		<script src="../Js/banner.js"></script>
		<link rel="stylesheet" type="text/css" href="../Css/bannercss.css" />
		<link rel="stylesheet" type="text/css" href="../Css/style1.css" />
		
	</head>
	<body>
		<header>
			<img src="../img/logo.png" />
			<ul>
				<li><a href="index1.jsp">首页</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">填写问卷</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.jsp">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
		</header>
		<div id="banner">
				<ul>
					<li class="on">1</li>
					<li>2</li>
					<li>3</li>
				</ul>
				<div id="banner_list">
					<a href="#" target="_blank"><img src="../img/banner1.png"   /></a>
					<a href="#" target="_blank"><img src="../img/banner2.png"   /></a>
					<a href="#" target="_blank"><img src="../img/banner3.png"   /></a>
				</div>
		</div>
		<div id="introduce">
			<img src="../img/tup1.png" />
			<p>&nbsp;&nbsp;&nbsp;&nbsp;问卷调查是指通过制定详细周密的问卷，要求被调查者据此进行回答以收集资料<br />
				的方法。所谓问卷是一组与研究目标有关的问题，或者说是一份为进行调查而编制的问<br />
				题表格，又称调查表。它是人们在社会调查研究活动中用来收集资料的一种常用工具。<br />
				调研人员借助这一工具对社会活动过程进行准确、具体的测定，并应用社会学统计方法<br />
				进行量的描述和分析，获取所需要的调查资料。</p>
		</div>
		<footer>
			<p>HXPZLQ版权所有</p>
		</footer>
	</body>
</html>