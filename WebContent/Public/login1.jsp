<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
		<meta charset="UTF-8">
		<title>登录页</title>
		<link rel="stylesheet" type="text/css" href="../Css/logincss.css" />
	</head>
	<style>
		#tipname{
			padding-left:88px;
		}
		#tippsw{
			padding-left:88px;
		}
	</style>
	<body>
		<header>
			<img src="../img/logo.png" />
			<ul>
				<li><a href="index1.jsp">首页</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">填写问卷</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.jsp">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
		</header>
		<div id="log">
			<div id="logg">
					<p><a href="login1.jsp" style="color: red;">用户登录</a></p>
					<p>|</p>
					<p><a href="login2.jsp" id="a2">管理员登录</a></p>
			</div>
			<form action="http://localhost:8080/research/userLoginServlet" method="post" id="form">
			<table>
				<tr>
					<td><input id="username" name="id" type="tel" maxlength="11" placeholder="  请输入电话号码" onclick="checkPsw()"/></td>
				</tr>
				<tr>
                	<td id="tipname" style="color:red;"></td>
				</tr>
				<tr>
					<td><input id="password" name="psw" type="password" maxlength="10" placeholder="  请输入密码" onclick="checkName()"/></td>
				</tr>
				<tr>
                	<td id="tippsw" style="color:red;"></td>
				</tr>
			</table>	
				<input type="button" value="登录" onclick="check()" id="denglu">
			</form>
			<p id="cp3"><a href="http://localhost:8080/research/Security/forgetPsw.jsp">忘记密码 | 找回密码</a></p>
		</div>
		<footer>
			<p>HXPZLQ版权所有</p>
		</footer>
	</body>
<script type="text/javascript">
function checkName(){
	var username = document.getElementById('username').value;
	if(username == ''){
		document.getElementById('tipname').innerHTML = "用户名不能为空";
		return;
	}else{
		document.getElementById('tipname').innerHTML = "";
		return;
	}
}
function checkPsw(){
	var username = document.getElementById('username').value;
	var password = document.getElementById('password').value;
	if(username!==''||password == ''){
       document.getElementById('tippsw').innerHTML = "密码不能为空";
       return;
	}else{
		document.getElementById('tippsw').innerHTML = "";
		return;
	}
}
function check(){
	var username = document.getElementById('username').value;
	var password = document.getElementById('password').value;
	if(username== ''){
       alert("用户名不能为空！");
       return;
   }else if(password== ''){
		alert("密码不能为空！");
		return;
	}
   else{
		var form=document.getElementById('form');
		form.submit();
		return;
	}
}
</script>
</html>