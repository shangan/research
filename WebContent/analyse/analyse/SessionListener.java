package analyse;

import javax.servlet.http.HttpSessionEvent;

import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {
	static int count;


	public void sessionCreated(HttpSessionEvent arg0) {
	count++;
	}


	public void sessionDestroyed(HttpSessionEvent arg0) {
	count--;
	}


	/**
	* @return the count
	*/
	public static  int getCount() {
	return count;
	}
}
