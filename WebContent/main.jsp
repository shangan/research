<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ page isELIgnored="false" %>  <!-- 2.4版本默认启用el表达式，如果使用2.5版本，默认el表达式是关闭的 -->
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>问卷调查</title>
<style type="text/css">
	header{
		width: 100%;
		height: 50px;
		background: #272b37;
		}
	header img{
		width: 120px;
		height: 40px;
		margin-left: 360px;
		margin-top: 5px;
		float: left;
		}
	header ul li{
		float: left;
		color: white;
		font-family: "é»ä½";
		font-size: 18px;
		text-align: center;
		margin-left: 20px;
		width: 100px;
		line-height: 50px;
		list-style: none;
		}
	header ul li:hover{
		color: #272b37;
		background: white;
		}
	#lr{
		float: right;
		margin-right: 350px;
		}
	#lr a{
		float: left;
		line-height: 50px;
		color: white;
		font-family: "é»ä½";
		font-size: 18px;
		margin-right: 10px;
		text-decoration: none;
		}
	#lr p{
		float: left;
		line-height: 50px;
		color: white;
		font-family: "é»ä½";
		font-size: 18px;
		margin-right: 10px;
		text-decoration: none;
		}
	#lr a:hover{
			color: yellow;
		}
	footer{
				width: 100%;
				height: 70px;
				background: #272b37;
				box-sizing: border-box;
				
				bottom:0px;
				<!-- 触底 --> 
			}
	footer p{
				color: white;
				font-size: 14px;
				text-align: center;
				padding-top: 25px;
			}
	#main{
		width: 700px;
		padding: 10px 100px;
		margin: 20px auto;
		/*margin: 3px 100px 10px;*/
		box-shadow: 0px 0px 15px 5px #aaa;
		border-radius: 10px;
	}
	
	#main_id{
		width=70%;
		margin-left:15%;
		border-bottom: 1px solide rgb(219,219,219);
		background: url(img/bg.png);
		background-size: 600px 100%;
		border-radius: 5px 5px; 
		padding:10px;margin-top:25px;
	}
	#btn{
		width: 80px;
		height: 30px;
	}

</style>
<!--<link href="/Css/main.css" rel="stylesheet">-->
</head>
<body>
<header>
			<img src="img/logo.png" />
			<ul>
				<li>首页</li>
				<li>问卷查看</li>
				<li>联系我们</li>
			</ul>
			<div id="lr">
				<a href="http://localhost:8080/research/Public/index2.jsp?account=${userid}">退出</a>
			</div>
</header>

<!--此处为遍历题目-->
<form action="http://localhost:8080/research/SubmitAnswerServlet" method="post" id="main" style="position: relative;">
<h2 style="text-align: center;margin: 10px 28%">调查问卷</h2>
   <c:forEach var="ques" items="${items}">	
  	<div class="ques">
	    <p><i>${ques.quesid}: </i>  ${ques.ques}<br/></p>
		    <c:forEach var="option" items="${ques.list}">
		    	<c:if test="${not empty option.ans}">
			    <p><input type="radio" name="${option.quesid}" value="${option.anid}"/>${option.anid}:${option.ans}<br/></p>
			    </c:if>
		    </c:forEach> 
   	</div>	
   </c:forEach> 
 <input type="submit" value="问卷提交" id="btn" style="text-align: center;margin: 10px 47%;border-radius: 7px"/>
</form>
<!--上面为遍历题目-->

<footer>
<p>HXPZLQ版权所有</p>
</footer>
</body>
</html>