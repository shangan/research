<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="../Css/style.css" />
    <script type="text/javascript" src="../Js/jquery.js"></script>
    <script type="text/javascript" src="../Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="../Js/bootstrap.js"></script>
    <script type="text/javascript" src="../Js/ckform.js"></script>
    <script type="text/javascript" src="../Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
</head>
<body>
<form action="http://localhost:8080/research/SaveQuestion" method="post" class="definewidth m20">
<table class="table table-bordered table-hover m10">
 	<tr>
        <td class="tableleft">题目编号</td>
        <td><input type="text" name="queid"/></td>
    </tr>
    <tr>
        <td class="tableleft">题目内容</td>
        <td><input type="text" name="que"/></td>
    </tr>
    <tr>
        <td class="tableleft">答案A编号</td>
        <td><input type="text" name="anid1"/></td>
    </tr>
     <tr>
        <td class="tableleft">答案A</td>
        <td><input type="text" name="ans1"/></td>
    </tr>
    <tr>
        <td class="tableleft">答案B编号</td>
        <td><input type="text" name="anid2"/></td>
    </tr>
     <tr>
        <td class="tableleft">答案B</td>
        <td><input type="text" name="ans2"/></td>
    </tr>
    <tr>
        <td class="tableleft">答案C编号</td>
        <td><input type="text" name="anid3"/></td>
    </tr>
    <tr>
        <td class="tableleft">答案C</td>
        <td><input type="text" name="ans3"/></td>
    </tr>
    <tr>
        <td class="tableleft">答案D编号</td>
        <td><input type="text" name="anid4"/></td>
    </tr>
    <tr>
        <td class="tableleft">答案D</td>
        <td><input type="text" name="ans4"/></td>
    </tr>
    <tr>
        <td class="tableleft"></td>
        <td>
            <button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
        </td>
    </tr>
</table>
</form>
</body>
</html>
<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="http://localhost:8080/research/QueryQuestion";
		 });

    });
</script>