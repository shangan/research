<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="Css/style.css" />
    <script type="text/javascript" src="Js/jquery.js"></script>
    <script type="text/javascript" src="Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="Js/bootstrap.js"></script>
    <script type="text/javascript" src="Js/ckform.js"></script>
    <script type="text/javascript" src="Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
</head>
<body>
<form class="form-inline definewidth m20" action="http://localhost:8080/research/SelectQuestion" method="get">
    问题编号：
    <input type="text" name="menuname" id="menuname"class="abc input-default" placeholder="" value="">&nbsp;&nbsp; 
    <button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; <button type="button" class="btn btn-success" id="addnew">新增题目</button>
</form>
<table class="table table-bordered table-hover definewidth m10">
<thead>
    <tr>
        <th>问题号</th>
        <th>问题</th>
        <th>答案</th>
       
        <th>操作</th>
        
    </tr>
</thead>
      <c:forEach var="q" items="${list}">
    <tr>
    	<td>${q.queid }</td>
    	<td>${q.que}</td>
    	<td>
    		<c:forEach var="a" items="${q.alist}">
    			${a.ans}&nbsp;&nbsp;&nbsp;
    		</c:forEach>
    	</td>
    	<td><a href="http://localhost:8080/research/DeleteoneQuestion?queid=${q.queid}">删除</a>
    		<a href="http://localhost:8080/research/QueryoneQuestion?id=${q.queid}">编辑</a>
    	</td>
    	
    </tr>
    </c:forEach>


   </table>


</body>
</html>
<script>
    $(function () {
        

		$('#addnew').click(function(){

				window.location.href="http://localhost:8080/research/Menu/add.jsp";
		 });


    });
	
</script>