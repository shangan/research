<%@page language="java" import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="Css/style.css" />
    <script type="text/javascript" src="Js/jquery.js"></script>
    <script type="text/javascript" src="Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="Js/bootstrap.js"></script>
    <script type="text/javascript" src="Js/ckform.js"></script>
    <script type="text/javascript" src="Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
</head>
<body>

<body>
<form action="http://localhost:8080/research/UpdateQuestion" method="get" class="definewidth m20">
<input type="hidden" name="id" value="${menu.id}" />
<table class="table table-bordered table-hover m10">
    
    <tr>
        <td class="tableleft">问题号</td>
        <td><input type="text" name="queid" value="${q.queid}" readonly/></td>
    </tr>
     <tr>
        <td class="tableleft">问题</td>
        <td><input type="text" name="que" value="${q.que}" /></td>
    </tr>
  
    <tr>
        <td class="tableleft">答案</td>   
        <td>
  				<c:forEach var="a" items="${q.alist}" >
        			<input type="text" name="${a.anid}" value="${a.ans}" />
        		</c:forEach>

        </td>     
    </tr>
    
    
    
     <tr>
        <td class="tableleft">答案id</td>
        <td>
              
        	<c:forEach var="a" items="${q.alist}" >
        		<input type="text" name="anid" value="${a.anid }" readonly />
        	</c:forEach>
        
        </td> 
    </tr>
   
   
    <tr>
        <td class="tableleft"></td>
        <td>
            <button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
        </td>
    </tr>
 </form>
</table>

</body>
</html>
<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="http://localhost:8080/research/QueryQuestion";
		 });

    });
</script>