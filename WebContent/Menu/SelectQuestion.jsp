<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="Css/style.css" />
    <script type="text/javascript" src="Js/jquery.js"></script>
    <script type="text/javascript" src="Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="Js/bootstrap.js"></script>
    <script type="text/javascript" src="Js/ckform.js"></script>
    <script type="text/javascript" src="Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
</head>
<body>
<table class="table table-bordered table-hover definewidth m10">
    <thead>
    <tr>
        <th>问题标号</th>
        <th>问题内容</th>
        <th>答案A</th>
        <th>答案B</th>
        <th>答案C</th>
        <th>答案D</th>
    </tr>
    </thead>
   <!--<td>${a1.anw}</td>
		<td>${a2.anw}</td>
		<td>${a3.anw}</td>
		<td>${a4.anw}</td> --> 
	<tr>
		<td>${q.queid}</td>
		<td>${q.que}</td>
		<c:forEach var="a" items="${list }" >
			<td>${a.ans }</td>
		</c:forEach>
	</tr> 
	
	<tr>
	<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
	</tr>
</table>

</body>
</html>
<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="http://localhost:8080/research/QueryQuestion";
		 });

    });
</script>