<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="Css/logincss.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>密保找回密码</title>
</head>
<style>
	.input{
				font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 40px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;
			}
</style>
<body>
<header>
			<img src="img/logo.png" />
			<ul>
				<li><a href="Public/index1.html">首页</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">填写问卷</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.html">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
</header>
<div id="log">
	<form action="http://localhost:8080/research/PswServlet" method="post" id="form">
		<c:forEach items="${infoList }" var="info"> 
		<div id="logg">
			<p><a>请回答下列问题</a></p>
		</div>
			<input class="input" name="question" value="${info.question }">
			<input value="${info.answer }" type="hidden" id="correctanswer">
			<input value="${info.id }" type="hidden" name="id"><br>
			<input class="input" name="answer" id="answer" placeholder="请输入正确答案"><br>
		<input type="button" value="确定" onclick="check()" id="denglu">
		</c:forEach>
	</form>
</div>
</body>
<script type="text/javascript">
		function check(){
			var correctanswer=document.getElementById('correctanswer').value;
			var answer=document.getElementById('answer').value;
			if(answer==""){
				alert("答案不能为空！");
				return;
			}else if(answer==correctanswer){
				alert("回答正确！");
				var form=document.getElementById('form');
				form.submit();
				return;
			}else{
				alert("回答错误！");
				return;
			} 
			
		}
</script>
</html>