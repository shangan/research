<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="Css/logincss.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改密码</title>
</head>
<style>
	.input{
				font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 40px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;
			}
</style>
<body>
<header>
			<img src="img/logo.png" />
			<ul>
				<li><a href="Public/index1.html">首页</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">填写问卷</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.html">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
</header>
<div id="log">
	<form action="http://localhost:8080/research/PswRevise" method="post" id="form">
		<input class="input" name="id" value="${id }" type="hidden">
		<div id="logg">
			<p><a>请输入新密码</a></p>
		</div>
		<input class="input" name="password" type="password" id="password1" placeholder="请输入新密码">
		<input class="input" id="password2" type="password" placeholder="请再次输入">
		<input type="button" id="denglu" onclick="check()" value="保存">
	</form>
</div>
</body>
<script type="text/javascript">
		function check(){
			var password1=document.getElementById('password1').value;
			var password2=document.getElementById('password2').value;
			if(password1==''||password2==''){
				alert("密码不能为空！");
				return;
			}
			else if(password1!=password2){
				alert("请输入相同的密码！");
				return;
			}else{
				alert("修改成功！");
				var form=document.getElementById('form');
				form.submit();
				return;
			}
		}
</script>
</html>