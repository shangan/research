<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style type="text/css">
	#result{
		margin: 30px auto 10px;
		width: 200px;
		height:112px;
		border:1px solid #eee;
	}
	#result img{
		width: 200px;
	}
	input{	
		width: 70px;
		margin-top: 10px;
	}
	@-moz-document url-prefix() { input { width:65px; } }/*单独对火狐进行设置*/
	#content{
		border: 1px solid #e5e5e5;
		margin: 10px auto 10px;
		border-radius: 5px;
		box-shadow: 0 1px 2px;
		width:800px;
		height:630px;
	}
	#infotable{
		font-size:18px;
		margin-top:30px;
		width:300px;
		height:300px;
	}
	.word{
		font-size:25px;
	}
</style>
<head>
<link rel="stylesheet" type="text/css" href="Css/logincss.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>个人中心</title>
</head>
<body>
	<header>
			<img src="img/logo.png" />
			<ul>
				<li><a href="Public/index1.html">首页</a></li>
				<li><a href="#">填写问卷</a></li>
				<li><a href="#">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="Public/login1.jsp">登录</a>
				<p>|</p>
				<a href="Public/regist.jsp">注册</a>
			</div>
	</header>
<div id="content">
    <div id = "result"> 
    	<center><input id="pic" type="file" name = 'pic' accept = "image/*" onchange = "selectFile()"/></center>
    </div>
    <br><br>
		<center><a class="word">基本信息</a></center>
	<form action="http://localhost:8080/research/InfoEdit" method="post">
			<c:forEach items="${infoList }" var="info"> 
				<input value="${info.id }" name="id" type="hidden">
				<center><table id="infotable">
					<tr>
						<td>电话号码：</td><td>${info.id }</td>
					</tr>
					<tr>
						<td>用户名：</td><td>${info.name }</td>
					</tr>
					<tr>
						<td>性别：</td><td>${info.sex }</td>
					</tr>
					<tr>
						<td>生日：</td><td>${info.year }年${info.month }月${info.birthday }日</td>
					</tr>
					<tr>
						<td>年龄：</td><td>${info.age }</td>
					</tr>
					<tr>
					
						<td colspan=2 class="word"><center>账户安全</center></td>
					</tr>
					<tr>
						<td>邮箱：</td><td>${info.email }</td>
					</tr>
					<tr>
						<td>密保问题：</td><td>${info.question }</td>
					</tr>
				</table></center>
			</c:forEach>
		<center><input type="submit" value="修改信息" style="width: 100px;height: 30px;border-radius: 5px;"></center>
	</form>
</div>
</body>
<script>
var form = new FormData();
var url = '127.0.0.1:8080/'
function selectFile(){
    var files = document.getElementById('pic').files;
    console.log(files[0]);
    if(files.length == 0){
        return;
    }
    var file = files[0];
    var reader = new FileReader();
    console.log(reader);
    reader.readAsBinaryString(file);
    reader.onload = function(f){
        var result = document.getElementById("result");
        var src = "data:" + file.type + ";base64," + window.btoa(this.result);
        result.innerHTML = '<img src ="'+src+'"/>';
    }
    console.log('file',file);
    form.append('file',file);
    console.log(form.get('file'));
}
   var xhr = new XMLHttpRequest();
   function handIn(){
  console.log(form.get('file'));
  xhr.open("post", url, true);
  xhr.addEventListener("readystatechange", function() {  
      var result = xhr;  
      if (result.status != 200) { //error  
         console.log('上传失败', result.status, result.statusText, result.response);  
      }   
     else if (result.readyState == 4) { //finished  
          console.log('上传成功', result);  
      }  
});   
 }
</script>
</html>