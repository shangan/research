<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="Css/logincss.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改信息</title>
</head>
<style type="text/css">
	#content{
		border: 1px solid #e5e5e5;
		margin: 10px auto 10px;
		border-radius: 5px;
		box-shadow: 0 1px 2px;
		width:800px;
		height:800px;
	}
	#c1{
			color: #272b37;
			font-size: 20px;
			}
</style>
<body>
<header>
			<img src="img/logo.png" />
			<ul>
				<li><a href="Public/index1.html">首页</a></li>
				<li><a href="#">填写问卷</a></li>
				<li><a href="#">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.html">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
	</header>
<div id="content">
<br>
<center><h1>修改信息</h1></center>
<br><br>
<form action="http://localhost:8080/research/SaveInfo" method="post" id="form"> 
<c:forEach items="${infoList }" var="info"> 
	<center>
		<table>
	<tr>
		<td id="c1">电话号码</td><td><input value="${info.id }" name="id" style="font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;"></td>
	</tr>
	<tr>
		<td id="c1">用户名</td><td><input value="${info.name }" name="name" style="font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;"></td>
	</tr>
	<tr>
		<td id="c1">密码</td><td><input value="${info.password }" name="password" type="password" style="font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;"></td>
		<td id="c1"><a href="Security/XGPsw.jsp?id=${info.id }&psw=${info.password }">修改密码</a></td>
	</tr>
	<tr>
		<td id="c1">邮箱</td><td><input value="${info.email }" name="email" style="font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;"></td>
	</tr>
	<tr>
		<td id="c1">性别</td><td><input value="${info.sex }" name="sex" style="font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;"></td>
	</tr>
	</table></center>
	<center id="c1">
		生日：<select id="birth_year" name="year" style="width: 60px;height: 30px;">
			<option >${info.year }</option>
			<c:forEach var="i" begin="1899" end="2018"> 
				<option><c:out value="${i}"/></option>
			</c:forEach>
		</select>
		<select name="month" style="width: 60px;height: 30px;">
			<option>${info.month }</option>
			<c:forEach var="i" begin="1" end="12"> 
				<option><c:out value="${i}"/></option>
			</c:forEach>
		</select>
		<select name="day" style="width: 60px;height: 30px;">
			<option>${info.birthday }</option>
			<c:forEach var="i" begin="1" end="31"> 
				<option><c:out value="${i}"/></option>
			</c:forEach>
		</select></center>
	<input type="hidden" value="${info.age }" id="age" name="age"><br>
</c:forEach>
<br>
<center><h1 id="c1">账户安全设置</h1></center><br><br>
	<center><a id="c1">设置密保问题</a>
		<select name="question" id="c1" style="font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;">
		<option>${info.question }</option>
		<option>你最喜欢的老师是谁？</option>
		<option>你最喜欢的动漫人物是谁？</option>
		<option>你最喜欢的明星是谁？</option>
		<option>你小学的班主任是谁？</option>
	</select><br>
	<a id="c1">答案</a><input name="answer" id="answer" value="${info.answer }" style="font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 15px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;"></center>
	<center><button onclick="getAge()" style="width: 80px;height: 30px;border-radius: 5px;margin-top: 10px;">保存</button></center>
</form>
</div>
</body>
<script type="text/javascript">
	    function getAge(){
	    	var date=new Date();   
	    	var year=date.getFullYear();
	    	var brith=document.getElementById("birth_year").value;
	    	var yearvalue=parseInt(year);
	    	var birthvalue=parseInt(brith); 
	    	var age=(yearvalue-birthvalue);
	    	document.getElementById("age").value=age;
	    	document.getElementById("year").value=yearvalue;
	    	var form=document.getElementById('form');
	    	form.submit();
	    }
</script>
</html>